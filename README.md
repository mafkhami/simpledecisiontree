# Simple Decision Tree

Due to the use of `vbaDeveloper`, a Windows environment is highly recommended
for development. However, it's also possible to develop on OS X (or even other
environements), as long as you make sure to properly import the `*.bas` files
back into the `SimpleDecisionTree.xlam` project.

## Contribution guides

The repository has a submodule `vbaDeveloper`, which allows us to manage Excel
add-in code via git. To clone this repository along with `vbaDeveloper`, clone
with the `--recursive` flag, ie,

```sh
git clone --recursive https://bitbucket.org/teachingda/simpledecisiontree.git
```

### Setting up `vbaDeveloper` add-in

In some cases, the add-in binary located in the repo as
`vbaDeveloper/vbaDeveloper.xlam` might work out-of-the-box. If that doesn't
work -- with error messages of any sort, follow the directions below to 'build
from source'.

Build the `vbaDeveloper` add-in per the instructions found in
`src/Build.bas`:

1. Open a new workbook in excel, then open the VB editor (`Alt`+`F11`)  and from
the menu `File` > `Import`, import this file:
     * src/vbaDeveloper.xlam/Build.bas
2. From `Tools` > `References...` add
     * Microsoft Visual Basic for Applications Extensibility 5.3
     * Microsoft Scripting Runtime
3. Rename the project to `vbaDeveloper`
5. Enable programatic access to VBA:
     * In Excel, `File` > `Options` -> `Trust Center`, then
     `Trust Center Settings`, > `Macros`
     * Tick the box: `Enable programatic access to VBA`
     * In Excel 2010+: `Trust access to the vba project object model`
6. In VB Editor, press `F4`, then under `Microsoft Excel Objects`, select
`ThisWorkbook`. Set the property `IsAddin` to `TRUE`
7. In VB Editor, `File` > `Save Book1`; Save as `vbaDeveloper.xlam` in the directory `vbaDeveloper/`
8. Close Excel. Open Excel with a new workbook, then open the just saved
`vbaDeveloper.xlam`
9. Let `vbaDeveloper` import its own code by putting the cursor in the function
`testImport` and press `F5`

### Developing `SimpleDecisionTree.xlam`

`SimpleDecisionTree` can either be developed in Excel via VB Editor, or by
editing the text code base in the `src/` directory at root.
It is crucial that contributors take care to keep the two in sync.

*Given that this is meant to be a text-based version controlled repo, priority
will be given to the code base at `src/SimpleDecisionTree.xlam/` when there is
conflict between the code and the `SimpleDecisionTree.xlam` binary.*

This actually makes sense, since `git` will overwrite the binary with the
latest version during merge -- regardless of conflict, while the text will
always merge multiple changes. Hence, when in doubt, the binary should always be
built from the source.

#### Developing with Excel's VB Editor

Before making change, make sure that the binary `SimpleDecisionTree.xlam` file
that you are about to modify is synced with the code base in `src/`. You can do
this with `Add-ins` > `VbaDeveloper` > `Import code for ...` > `SimpleDecisionTree`.

After saving your changes to the script, make sure you export the changes to
the code base at `src/` by running `Add-ins` > `VbaDeveloper` > `Export code for ...` > `SimpleDecisionTree`.

#### Developing on the plaintext code base in `src/`

Before making change, make sure that the code base at `src/` files
that you are about to modify are synced with the code in the
`SimpleDecisionTree.xlam` binary. You can do
this by first running `Add-ins` > `VbaDeveloper` > `Export code for ...` > `SimpleDecisionTree` within Excel.

After saving your changes to the `*.bas` files, make sure you import the changes back to
the binary at `src/` by running `Add-ins` > `VbaDeveloper` > `Import code for ...` > `SimpleDecisionTree`.