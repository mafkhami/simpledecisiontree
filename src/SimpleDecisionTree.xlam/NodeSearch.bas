Attribute VB_Name = "NodeSearch"
    'Simple Decision Tree
    'https://bitbucket.org/teachingda/simpledecisiontree
    'Copyright (C) 2008  Thomas Seyller

    'This program is free software: you can redistribute it and/or modify
    'it under the terms of the GNU General Public License as published by
    'the Free Software Foundation, either version 3 of the License, or
    '(at your option) any later version.

    'This program is distributed in the hope that it will be useful,
    'but WITHOUT ANY WARRANTY; without even the implied warranty of
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    'GNU General Public License for more details.

    'You should have received a copy of the GNU General Public License
    'along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    '***************************************************************
    '*** Additional permission under GNU GPL version 3 section 7 ***
    '***************************************************************
    'As a special exception to the terms and conditions of GPL version 3.0,
    'Thomas Seyller hereby grants you the rights to link the
    'macros to functions calls that normally comes with the standard
    'built-in libraries provided by your office application programs.
    
    
    
    'This macro modified on 2/6/2009 by William VanHoomissen
                
    'This program is free software: you can redistribute it and/or modify
    'it under the terms of the GNU General Public License as published by
    'the Free Software Foundation, either version 3 of the License, or
    '(at your option) any later version.

    'This program is distributed in the hope that it will be useful,
    'but WITHOUT ANY WARRANTY; without even the implied warranty of
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    'GNU General Public License for more details.

    'You should have received a copy of the GNU General Public License
    'along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    '***************************************************************
    '*** Additional permission under GNU GPL version 3 section 7 ***
    '***************************************************************
    'As a special exception to the terms and conditions of GPL version 3.0,
    'Thomas Seyller hereby grants you the rights to link the
    'macros to functions calls that normally comes with the standard
    'built-in libraries provided by your office application programs.

Public Function Find_Node(node_nb) As Integer
'Finds and returns a node given its number, or -1 if not found

    Find_Node = Find_Shape("TrNd", node_nb)

End Function

Public Function Ancestor(shape_type, shape_nb) As Integer
'Returns the node number of the ancestor of a shape (node or leaf)
    
    If Len(shape_nb) = 0 Then
        Ancestor = -1
    Else
        Ancestor = Find_Node(Left(shape_nb, Len(shape_nb) - 1))
    End If

End Function

Public Function Find_Node_By_Row_Col(rw, col) As Integer
'Returns the node shape number of the node that is located near cell(rw, col)

    Dim ws As Worksheet
    Set ws = ActiveSheet
    
    Find_Node_By_Row_Col = -1
    
    For i = 1 To ws.Shapes.Count
        p = InStr(ws.Shapes(i).Name, "TrNd")
        srw = ws.Shapes(i).TopLeftCell.Row
        src = ws.Shapes(i).TopLeftCell.Column
        If IsNumeric(p) And p > 0 And (srw - rw) ^ 2 + (src - col) ^ 2 <= 2 Then
            Find_Node_By_Row_Col = i
            Exit Function
        End If
    Next i

End Function

Public Function Find_Leaf_By_Row_Col(rw, col) As Integer
'Returns the node shape number of the leaf that is located near cell(rw, col)

    Dim ws As Worksheet
    Set ws = ActiveSheet
    
    Find_Leaf_To_Delete = -1
    For i = 1 To ws.Shapes.Count
        p = InStr(ws.Shapes(i).Name, "Leaf")
        srw = ws.Shapes(i).TopLeftCell.Row
        src = ws.Shapes(i).TopLeftCell.Column
        If IsNumeric(p) And p > 0 And (srw - rw) ^ 2 + (src - col) ^ 2 <= 2 Then
            Find_Leaf_By_Row_Col = i
            Exit Function
        End If
    Next i

End Function

Public Function Find_Shape(shape_type, shape_nb) As Integer
'Finds and returns a shape (node, branch or leaf) given its shape type and number, or -1 if not found

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Find_Shape = -1
    
    For i = 1 To ws.Shapes.Count
        If ws.Shapes(i).Name = shape_type & " " & shape_nb Then
            Find_Shape = i
            Exit Function
        End If
    Next i

End Function
