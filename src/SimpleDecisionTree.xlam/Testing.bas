Attribute VB_Name = "Testing"
    'Simple Decision Tree
    'https://bitbucket.org/teachingda/simpledecisiontree
    'Copyright (C) 2008  Thomas Seyller

    'This program is free software: you can redistribute it and/or modify
    'it under the terms of the GNU General Public License as published by
    'the Free Software Foundation, either version 3 of the License, or
    '(at your option) any later version.

    'This program is distributed in the hope that it will be useful,
    'but WITHOUT ANY WARRANTY; without even the implied warranty of
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    'GNU General Public License for more details.

    'You should have received a copy of the GNU General Public License
    'along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    '***************************************************************
    '*** Additional permission under GNU GPL version 3 section 7 ***
    '***************************************************************
    'As a special exception to the terms and conditions of GPL version 3.0,
    'Thomas Seyller hereby grants you the rights to link the
    'macros to functions calls that normally comes with the standard
    'built-in libraries provided by your office application programs.

Public Function Exists_Tree() As Boolean
'Verifies whether there already exists a tree in the active worksheet

    Dim ws As Worksheet
    Dim c As Range
    Set ws = ActiveSheet
    Set c = ActiveCell
    Exists_Tree = False
    
    For Each s In ws.Shapes
        p = InStr(s.Name, "TrNd")
        If IsNumeric(p) And p > 0 Then
            Exists_Tree = True
            Exit For
        End If
    Next s

End Function

Public Function Is_Admissible_Cell() As Long
'Verifies whether the active cell is admissible to create a new node;
'If it is, it returns the number that should be assigned to the new node, otherwise it returns 0

    Dim ws As Worksheet
    Dim c As Range
    Set ws = ActiveSheet
    Set c = ActiveCell
    rw = c.Row
    col = c.Column
    Is_Admissible_Cell = 0
    
    For Each s In ws.Shapes
        p = InStr(s.Name, "Leaf")
        If IsNumeric(p) And p > 0 And s.TopLeftCell.Row = rw And s.TopLeftCell.Column = col Then
            Is_Admissible_Cell = Extract_Number(s.Name)
            Exit For
        End If
    Next s

End Function

Public Function Extract_Number(s) As String
'Extracts the node / leaf number from the name of a node / leaf

    l = Len(s)
    p = InStr(s, "TrNd")
    If IsNumeric(p) And p > 0 Then
        Extract_Number = Right(s, l - 5)
    Else
        Extract_Number = Right(s, l - 5)
    End If
    
End Function
