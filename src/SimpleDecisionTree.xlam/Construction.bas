Attribute VB_Name = "Construction"
    'Simple Decision Tree
    'https://bitbucket.org/teachingda/simpledecisiontree
    'Copyright (C) 2008  Thomas Seyller

    'This program is free software: you can redistribute it and/or modify
    'it under the terms of the GNU General Public License as published by
    'the Free Software Foundation, either version 3 of the License, or
    '(at your option) any later version.

    'This program is distributed in the hope that it will be useful,
    'but WITHOUT ANY WARRANTY; without even the implied warranty of
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    'GNU General Public License for more details.

    'You should have received a copy of the GNU General Public License
    'along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    '***************************************************************
    '*** Additional permission under GNU GPL version 3 section 7 ***
    '***************************************************************
    'As a special exception to the terms and conditions of GPL version 3.0,
    'Thomas Seyller hereby grants you the rights to link the
    'macros to functions calls that normally comes with the standard
    'built-in libraries provided by your office application programs.

Public Function Find_Branch(branch_nb) As Integer
'Finds and returns a branch given its number, or -1 if not found

    Find_Branch = Find_Shape("Branch", branch_nb)

End Function

Public Function Max_Stage() As Integer
'Returns the number of stages of distinctions present in the tree; returns -1 if no tree is present

    Dim ws As Worksheet
    Set ws = ActiveSheet
    
    If Testing.Exists_Tree = False Then
        Max_Stage = -1
        Exit Function
    End If
    Max_Stage = 0
    
    For Each s In ws.Shapes
    p = InStr(s.Name, "Leaf")
        If IsNumeric(p) And p > 0 Then
            leaf_stage = Len(s.Name) - 5
            If leaf_stage > Max_Stage Then Max_Stage = leaf_stage
        End If
    Next s

End Function

Public Function Max_Stage2() As Integer
'Returns the number of stages of distinctions present in the tree; returns -1 if no tree is present

    Dim ws As Worksheet
    Set ws = ActiveSheet
    
    If Testing.Exists_Tree = False Then
        Max_Stage2 = -1
        Exit Function
    End If
    Max_Stage2 = 0
    
    For Each s In ws.Shapes
    p = InStr(s.Name, "TrNd")
        If IsNumeric(p) And p > 0 Then
            leaf_stage = Len(s.Name) - 4
            If leaf_stage > Max_Stage2 Then Max_Stage2 = leaf_stage
        End If
    Next s

End Function

Public Sub Reposition_Nodes(node_nb)
'Repositions a specified node, given its node number, and all of its ancestor nodes

    Call Reposition_Node(node_nb)
    If Len(node_nb) = 0 Then Exit Sub
    Call Reposition_Nodes(Left(node_nb, Len(node_nb) - 1))
    
End Sub

Public Sub Tree_Values_No_Repositioning(node_nb)
'Calculates the u-value at a specified node, given its node number, and at all ancestor nodes

    Call Tree_U_Value(node_nb)
    Call Tree_Value_Measure(node_nb)
    If Len(node_nb) = 0 Then Exit Sub
    Call Tree_Values_No_Repositioning(Left(node_nb, Len(node_nb) - 1))
    
End Sub

Public Function Is_Child_Node(s, node_nb) As Boolean
'Checks whether a specific shape s is a node and is a child of node number node_nb

    p = InStr(s.Name, "TrNd")
    If IsNumeric(p) And p > 0 And Len(s.Name) = Len(node_nb) + 6 Then
        pp = InStr(s.Name, "TrNd " & node_nb)
        If IsNumeric(pp) And pp > 0 Then
            Is_Child_Node = True
        Else
            Is_Child_Node = False
        End If
    Else
        Is_Child_Node = False
    End If

End Function

Public Function Is_Child_Leaf(s, node_nb) As Boolean
'Checks whether a specific shape s is a leaf and is a child of node number node_nb

    p = InStr(s.Name, "Leaf")
    If IsNumeric(p) And p > 0 And Len(s.Name) = Len(node_nb) + 6 Then
        pp = InStr(s.Name, "Leaf " & node_nb)
        If IsNumeric(pp) And pp > 0 Then
            Is_Child_Leaf = True
        Else
            Is_Child_Leaf = False
        End If
    Else
        Is_Child_Leaf = False
    End If

End Function

Public Sub Reposition_Node(node_nb)
'Reposition a node and the branch leading to it given its number

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim node As Shape
    Dim branch As Shape

    Set node = ws.Shapes(Find_Node(node_nb))
    min_max_branch_rows_exist = False
    max_branch_row = 0
    min_branch_row = 65535
    'Find the highest and lowest branches that are rooted in this node
    For Each s In ws.Shapes
        p = InStr(s.Name, "Branch " & node_nb)
        If p = 1 And Len(s.Name) = 8 + Len(node_nb) Then
            branch_row = s.TopLeftCell.Row
            If min_max_branch_rows_exist = False Then
                min_max_branch_rows_exist = True
                min_branch_row = branch_row
                max_branch_row = branch_row
            Else
                If min_branch_row > branch_row Then
                    min_branch_row = branch_row
                Else
                    If max_branch_row < branch_row Then
                        max_branch_row = branch_row
                    End If
                End If
            End If
        End If
    Next s
    If min_max_branch_rows_exist = False Then
        new_node_row = node.TopLeftCell.Row
    Else
        new_node_row = Round(min_branch_row + (max_branch_row - min_branch_row) / 2)
    End If
    old_node_row = node.TopLeftCell.Row
    'If new_node_row <> old_node_row Then
        'Call Delete_Tree_Value(node_nb)
        'Need to readjust the position of the node...
        'node.Top = ws.Cells(new_node_row, 1).Top
        '... and of the branch leading to it
        right_col = node.TopLeftCell.Column
        If node_nb = "" Then
            left_col = right_col - 1
            'Set branch = ws.Shapes(Find_Shape("Root", node_nb))
            'branch.Top = ws.Cells(new_node_row, 1).Top + ws.Cells(new_node_row, 1).height / 2
        Else
            left_col = right_col - 4
            'auxcol = node.TopLeftCell.Column
            'If ws.Cells(old_node_row - 1, auxcol - 2).Formula = "" Then
            '    row_adjust = 2
            'Else
            '    row_adjust = 0
            'End If
            'old_node_row = old_node_row - row_adjust
            'ws.Cells(new_node_row - 1, auxcol - 3).Formula = ws.Cells(old_node_row - row_adjust - 1, auxcol - 3).Formula
            'ws.Cells(new_node_row - 1, auxcol - 3).Font.Bold = ws.Cells(old_node_row - row_adjust - 1, auxcol - 3).Font.Bold
            'ws.Cells(new_node_row - 1, auxcol - 3).NumberFormat = ws.Cells(old_node_row - row_adjust - 1, auxcol - 3).NumberFormat
            'ws.Cells(new_node_row - 1, auxcol - 2).Formula = ws.Cells(old_node_row - row_adjust - 1, auxcol - 2).Formula
            'ws.Cells(new_node_row - 1, auxcol - 2).Font.Bold = ws.Cells(old_node_row - row_adjust - 1, auxcol - 2).Font.Bold
            'ws.Cells(old_node_row - row_adjust - 1, auxcol - 3).Formula = ""
            'ws.Cells(old_node_row - row_adjust - 1, auxcol - 2).Formula = ""
            'Set branch = ws.Shapes(Find_Shape("Branch", node_nb))
            'branch.Top = ws.Cells(new_node_row, 1).Top + ws.Cells(new_node_row, 1).height / 2
            'Set branch = ws.Shapes(Find_Shape("FBranch", node_nb))
            'Call New_FBranch(branch.Left, branch.Left + branch.width, ws.Shapes(Ancestor("TrNd", node_nb)).Top + ws.Shapes(Ancestor("TrNd", node_nb)).height / 2, ws.Cells(new_node_row, 1).Top + ws.Cells(new_node_row, 1).height / 2, node_nb)
        End If
        If new_node_row > old_node_row Then
            For i = 1 To new_node_row - old_node_row
                ws.Range(ws.Cells(old_node_row - 1, left_col), ws.Cells(old_node_row - 1, right_col)).Insert (xlShiftDown)
                ws.Range(ws.Cells(new_node_row + 1, left_col), ws.Cells(new_node_row + 1, right_col)).Delete (xlShiftUp)
            Next i
        Else
            For i = 1 To new_node_row - old_node_row
                ws.Range(ws.Cells(old_node_row - 1, left_col), ws.Cells(old_node_row - 1, right_col)).Insert (xlShiftDown)
                ws.Range(ws.Cells(new_node_row + 1, left_col), ws.Cells(new_node_row + 1, right_col)).Delete (xlShiftUp)
            Next i
        End If
        'Need to readjust the starting point of all front arcs of the branches starting from the node
        For Each s In ws.Shapes
            p = InStr(s.Name, "FBranch " & node_nb)
            If p > 0 And Len(s.Name) = 9 + Len(node_nb) Then
                Call New_FBranch(s.Left, s.Left + s.width, node.Top + node.height / 2, ws.Shapes(Find_Shape("Branch", Right(s.Name, Len(s.Name) - 8))).Top, Right(s.Name, Len(s.Name) - 8))
            End If
        Next s
    'End If

End Sub
