Attribute VB_Name = "FlipTree"
Public Function flip()
    'flip function
    'Generates the tree on current sheet in tabular form, then
    'flips, and rolls back the tree in tabular form, based on
    'number of layers of branches in the tree. Two and Three
    'layer tress are allowed with two or three branches per node.
    
    'Function written 8/7/2009 by William VanHoomissen
    'Function written as feature addition to Simple Decision
    'Tree, originally by Thomas Seyller
    
    'Simple Decision Tree
    'https://bitbucket.org/teachingda/simpledecisiontree
    'Copyright (C) 2008  Thomas Seyller, William VanHoomissen

    'This program is free software: you can redistribute it and/or modify
    'it under the terms of the GNU General Public License as published by
    'the Free Software Foundation, either version 3 of the License, or
    '(at your option) any later version.

    'This program is distributed in the hope that it will be useful,
    'but WITHOUT ANY WARRANTY; without even the implied warranty of
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    'GNU General Public License for more details.

    'You should have received a copy of the GNU General Public License
    'along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    '***************************************************************
    '*** Additional permission under GNU GPL version 3 section 7 ***
    '***************************************************************
    'As a special exception to the terms and conditions of GPL version 3.0,
    'Thomas Seyller, William VanHoomissen hereby grants you the rights to link the
    'macros to functions calls that normally comes with the standard
    'built-in libraries provided by your office application programs.

    
' ****************** check current tree for format *********************
    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim Msg, Style, Title, Help, Ctxt, Response, MyString

' check for decision nodes
    For i = 1 To ws.Shapes.Count
'        Cells(i, 19) = ws.Shapes(i).Name
'        Cells(i, 20) = ws.Shapes(i).AutoShapeType
        If (ws.Shapes(i).AutoShapeType = msoShapeRectangle) Then
            Msg = "A tree containing decisions cannot be flipped"
            Style = vbInformation
            Title = "Format error"
            responce = MsgBox(Msg, Style, Title)
            Return
        End If
    Next i

' check for more than three layers
        If NodeSearch.Find_Shape("Branch", 1111) > -1 Then
            Msg = "No more than 3 layers of uncertainty allowed"
            Style = vbInformation
            Title = "Format error"
            responce = MsgBox(Msg, Style, Title)
            Return
        End If

' map the tree
        first_count = 0
        second_count = 0
        third_count = 0
        
        For i = 1 To 3
            If NodeSearch.Find_Shape("Branch", i) > -1 Then first_count = i
            If NodeSearch.Find_Shape("Branch", 1 & i) > -1 Then second_count = i
            If NodeSearch.Find_Shape("Branch", 11 & i) > -1 Then third_count = i
        Next i

' ******************** calculate alternative probabilities ************************

Dim likelihood(3, 3, 3) As Double
Dim outcomes(3, 3) As String

a = get_likelihoods(likelihood(), outcomes(), first_count, second_count, third_count)

'********************* rebuild tree in table form *********************************

' create and label new sheet
    Dim new_sheet As String
    new_sheet = ws.Name & " - flipped"
    Sheets.Add
    
    Set ws = ActiveSheet
    ws.Name = new_sheet
    Cells.Select
    Selection.Borders.ColorIndex = 2
    Selection.NumberFormat = "0.000"
    
    Dim temp_likelihood(3, 3, 3) As Double
    Dim temp_outcomes(3, 3) As String
    activeRow = 1
    
' check for number of tables required
    If third_count > 0 Then
        loop_third_count = third_count
        iterations = 6  ' 6 tables required
    Else:
        loop_third_count = 1
        iterations = 2  ' 2 tables required
    End If
    
' load variables for table building function
    For Order = 1 To iterations
        For i = 1 To first_count
            For j = 1 To second_count
                For k = 1 To loop_third_count
                    Select Case Order
                        Case 1:
                        
                            note = "Original Order"
                            temp_likelihood(i, j, k) = likelihood(i, j, k)
                            
                            temp_first_count = first_count
                            temp_second_count = second_count
                            temp_third_count = third_count
                            
                            temp_outcomes(1, i) = outcomes(1, i)
                            temp_outcomes(2, j) = outcomes(2, j)
                            temp_outcomes(3, k) = outcomes(3, k)
                            
                            color1 = 34
                            color2 = 35
                            color3 = 36
                        
                        Case 2:
                        
                            note = "Flipped #1"
                            temp_likelihood(j, i, k) = likelihood(i, j, k)
                            
                            temp_first_count = second_count
                            temp_second_count = first_count
                            temp_third_count = third_count
                            
                            temp_outcomes(2, i) = outcomes(1, i)
                            temp_outcomes(1, j) = outcomes(2, j)
                            temp_outcomes(3, k) = outcomes(3, k)
                            
                            color1 = 35
                            color2 = 34
                            color3 = 36
                        
                        Case 3:
                            
                            note = "Flipped #2"
                            temp_likelihood(i, k, j) = likelihood(i, j, k)
                            
                            temp_first_count = first_count
                            temp_second_count = third_count
                            temp_third_count = second_count
                            
                            temp_outcomes(1, i) = outcomes(1, i)
                            temp_outcomes(3, j) = outcomes(2, j)
                            temp_outcomes(2, k) = outcomes(3, k)
                            
                            color1 = 34
                            color2 = 36
                            color3 = 35
                        
                        Case 4:
                        
                            note = "Flipped #3"
                            temp_likelihood(k, i, j) = likelihood(i, j, k)
                            
                            temp_first_count = third_count
                            temp_second_count = first_count
                            temp_third_count = second_count
                            
                            temp_outcomes(2, i) = outcomes(1, i)
                            temp_outcomes(3, j) = outcomes(2, j)
                            temp_outcomes(1, k) = outcomes(3, k)
                            
                            color1 = 36
                            color2 = 34
                            color3 = 35
                        
                        Case 5:
                            
                            note = "Flipped #4"
                            temp_likelihood(k, j, i) = likelihood(i, j, k)
                            
                            temp_first_count = third_count
                            temp_second_count = second_count
                            temp_third_count = first_count
                            
                            temp_outcomes(3, i) = outcomes(1, i)
                            temp_outcomes(2, j) = outcomes(2, j)
                            temp_outcomes(1, k) = outcomes(3, k)
                            
                            color1 = 36
                            color2 = 35
                            color3 = 34
                        
                        Case 6:
                            
                            note = "Flipped #5"
                            temp_likelihood(j, k, i) = likelihood(i, j, k)
        
                            temp_first_count = second_count
                            temp_second_count = third_count
                            temp_third_count = first_count
                            
                            temp_outcomes(3, i) = outcomes(1, i)
                            temp_outcomes(1, j) = outcomes(2, j)
                            temp_outcomes(2, k) = outcomes(3, k)
                            
                            color1 = 35
                            color2 = 36
                            color3 = 34
                            
                    End Select
                Next k
            Next j
        Next i

' build the table
        activeRow = build_table(temp_outcomes(), temp_likelihood(), temp_first_count, temp_second_count, temp_third_count, activeRow + 2, color1, color2, color3, note)
    
    Next Order
    
    Cells(1, 1).Select

End Function
 
Public Function build_table(ByRef outcomes() As String, ByRef temp_likelihood() As Double, first_count, second_count, third_count, activeRow, color1, color2, color3, note)
    Dim ws As Worksheet
    Set ws = ActiveSheet
    
    ws.Cells(activeRow, 1).Value = note
    activeRow = activeRow + 1
    
    For first_layer = 1 To first_count
        
        ws.Cells(activeRow, 1).Value = outcomes(1, first_layer)
        total1_row = activeRow
        
        For second_layer = 1 To second_count
            
            ws.Cells(activeRow, 3).Value = outcomes(2, second_layer)
            total2_row = activeRow
            
            If third_count > 1 Then
                For third_layer = 1 To third_count

                    Cells(activeRow, 5).Value = outcomes(3, third_layer)
                    Range(Cells(activeRow, 5), Cells(activeRow, 6)).Interior.ColorIndex = color3
                    Range(Cells(activeRow, 5), Cells(activeRow, 6)).Borders.ColorIndex = 1
                    
                    ws.Cells(activeRow, 6).Select
                    ActiveCell.FormulaR1C1 = "=(RC[1]/(R[" & -(activeRow - total1_row) & "]C[-4] * R[" & -(activeRow - total2_row) & "]C[-2]))"
                    
                    ws.Cells(activeRow, 7).Value = temp_likelihood(first_layer, second_layer, third_layer)
                    ws.Cells(activeRow, 7).Font.ColorIndex = 15
                    
                    activeRow = activeRow + 1
                Next third_layer
            Else:   ws.Cells(activeRow, 5).Value = temp_likelihood(first_layer, second_layer, 1)
                    ws.Cells(activeRow, 5).Font.ColorIndex = 15
                    activeRow = activeRow + 1
            End If
        
        ws.Cells(total2_row, 4).Select
        
        If third_count > 1 Then
                ActiveCell.FormulaR1C1 = "=(SUM(RC[" & 3 & "]:R[" & (activeRow - total2_row) - 1 & "]C[3])/R[" & -(total2_row - total1_row) & "]C[-2])"
        Else:   ActiveCell.FormulaR1C1 = "=(SUM(RC[" & 1 & "]:R[" & (activeRow - total2_row) - 1 & "]C[1])/R[" & -(total2_row - total1_row) & "]C[-2])"
        End If
        
        Range(Cells(total2_row, 3), Cells(activeRow - 1, 3)).Merge
        Range(Cells(total2_row, 4), Cells(activeRow - 1, 4)).Merge
        Range(Cells(total2_row, 3), Cells(activeRow - 1, 4)).Interior.ColorIndex = color2
        Range(Cells(total2_row, 3), Cells(activeRow - 1, 4)).Borders.ColorIndex = 1
        
        Next second_layer
    
    ws.Cells(total1_row, 2).Select
    
    If third_count > 1 Then
            ActiveCell.FormulaR1C1 = "=(SUM(RC[" & 5 & "]:R[" & (activeRow - total1_row) - 1 & "]C[5]))"
    Else:   ActiveCell.FormulaR1C1 = "=(SUM(RC[" & 3 & "]:R[" & (activeRow - total1_row) - 1 & "]C[3]))"
    End If
        
    Range(Cells(total1_row, 1), Cells(activeRow - 1, 1)).Merge
    Range(Cells(total1_row, 2), Cells(activeRow - 1, 2)).Merge
    Range(Cells(total1_row, 1), Cells(activeRow - 1, 2)).Interior.ColorIndex = color1
    Range(Cells(total1_row, 1), Cells(activeRow - 1, 2)).Borders.ColorIndex = 1
    
    Next first_layer

' Format table
    ws.Columns.AutoFit
    Cells.VerticalAlignment = xlCenter

' Return activeRow
    build_table = activeRow

End Function
Public Function get_likelihoods(ByRef likelihood() As Double, ByRef outcomes() As String, first_count, second_count, third_count)
    Dim ws As Worksheet
    Set ws = ActiveSheet
    third_layer = 1

    For first_layer = 1 To first_count
        Set branch = ws.Shapes(Find_Shape("Branch", first_layer))
        rw_branch = branch.TopLeftCell.Row
        temp1 = ws.Cells(rw_branch - 1, branch.TopLeftCell.Column + 0).Value
        outcomes(1, first_layer) = ws.Cells(rw_branch - 1, branch.TopLeftCell.Column + 1).Value
        
        For second_layer = 1 To second_count
            Set branch = ws.Shapes(Find_Shape("Branch", first_layer & second_layer))
            rw_branch = branch.TopLeftCell.Row
            temp2 = ws.Cells(rw_branch - 1, branch.TopLeftCell.Column + 0).Value
            outcomes(2, second_layer) = ws.Cells(rw_branch - 1, branch.TopLeftCell.Column + 1).Value
        
            If third_count > 0 Then
                For third_layer = 1 To third_count
                    Set branch = ws.Shapes(Find_Shape("Branch", first_layer & second_layer & third_layer))
                    rw_branch = branch.TopLeftCell.Row
                    temp3 = ws.Cells(rw_branch - 1, branch.TopLeftCell.Column + 0).Value
                    outcomes(3, third_layer) = ws.Cells(rw_branch - 1, branch.TopLeftCell.Column + 1).Value
                
                    likelihood(first_layer, second_layer, third_layer) = temp1 * temp2 * temp3
            
                Next third_layer
            Else:   likelihood(first_layer, second_layer, third_layer) = temp1 * temp2
            End If
        Next second_layer
    Next first_layer

End Function
