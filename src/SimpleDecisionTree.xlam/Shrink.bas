Attribute VB_Name = "Shrink"
'Simple Decision Tree
    'https://bitbucket.org/teachingda/simpledecisiontree
    'Copyright (C) 2008  Thomas Seyller

    'This program is free software: you can redistribute it and/or modify
    'it under the terms of the GNU General Public License as published by
    'the Free Software Foundation, either version 3 of the License, or
    '(at your option) any later version.

    'This program is distributed in the hope that it will be useful,
    'but WITHOUT ANY WARRANTY; without even the implied warranty of
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    'GNU General Public License for more details.

    'You should have received a copy of the GNU General Public License
    'along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    '***************************************************************
    '*** Additional permission under GNU GPL version 3 section 7 ***
    '***************************************************************
    'As a special exception to the terms and conditions of GPL version 3.0,
    'Thomas Seyller hereby grants you the rights to link the
    'macros to functions calls that normally comes with the standard
    'built-in libraries provided by your office application programs.
      
    'This macro modified on 2/9/2009 by William VanHoomissen
    '   add "Shrink" function
                
    'This program is free software: you can redistribute it and/or modify
    'it under the terms of the GNU General Public License as published by
    'the Free Software Foundation, either version 3 of the License, or
    '(at your option) any later version.

    'This program is distributed in the hope that it will be useful,
    'but WITHOUT ANY WARRANTY; without even the implied warranty of
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    'GNU General Public License for more details.

    'You should have received a copy of the GNU General Public License
    'along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    '***************************************************************
    '*** Additional permission under GNU GPL version 3 section 7 ***
    '***************************************************************
    'As a special exception to the terms and conditions of GPL version 3.0,
    'Thomas Seyller hereby grants you the rights to link the
    'macros to functions calls that normally comes with the standard
    'built-in libraries provided by your office application programs.


Public Function Shrink_Tree() As Integer
'Finds rows within the tree that are blank and deletes them

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim i As Variant
    Dim ii As String

'  warn that this can't be undone

        Dim Msg, Style, Title, Help, Ctxt, Response, MyString
        Msg = "Shrinking tree cannot be undone.  Do you want to continue?"    ' Define message.
        Style = vbYesNo + vbDefaultButton2     ' Define buttons.+ vbCritical
        Title = "Shrink Tree"    ' Define title.
        Help = "DEMO.HLP"    ' Define Help file.
        Ctxt = 1000     ' Define topic
                        ' context.
                        ' Display message.
        Response = MsgBox(Msg, Style, Title, Help, Ctxt)
        If Response = vbYes Then

        Else
            Exit Function
        End If

    
' Variables for first and last row in the tree
max_ro = 0
min_ro = 1000

' Find first and last rows in the tree by looking
' for highest and lowest rows of all shapes.
For i = 1 To ws.Shapes.Count
    
    ro = ws.Shapes(i).TopLeftCell.Row
    
    If ro > max_ro Then
        max_ro = ro
        End If
    
    If ro < min_ro Then
        min_ro = ro
        End If
Next i

' For diagnositics
' List row, column, and name of every shape in the tree
'For i = 1 To ws.Shapes.Count ' print shape data base
'        ii = i
'        Range("R" & ii).Select
'        ActiveCell.FormulaR1C1 = i
'        Range("S" & ii).Select
'        ActiveCell.FormulaR1C1 = ws.Shapes(i).TopLeftCell.Column
'        Range("T" & ii).Select
'        ActiveCell.FormulaR1C1 = ws.Shapes(i).TopLeftCell.Row
'        Range("U" & ii).Select
'        ActiveCell.FormulaR1C1 = ws.Shapes(i).Name
'Next i

' move the CE value of the tree (always two rows down from shape #1)
' to clear row for deletion
    j = 1
    ro_check = ws.Shapes(j).TopLeftCell.Row
    ws.Cells(ro_check + 2, ws.Shapes(j).TopLeftCell.Column).Cut
    ActiveSheet.Paste Destination:=ws.Cells(ro_check + 1, ws.Shapes(j).TopLeftCell.Column + 1)

' Check each row, starting from bottom of tree, and see if it is blank
For i = max_ro To min_ro Step -1
    ii = i
    delete_ro = 1 ' default is set to delete every row
    replace_needed = 0
    
' check each shape to see if it is within one row of the active row
    For j = 1 To ws.Shapes.Count
        ro_check = ws.Shapes(j).TopLeftCell.Row
        
        If ro_check = i Then ' Is there a shape on this row?
            delete_ro = 0   ' Yes, so cann't delet row
        End If
        If ro_check = i + 1 Then  ' Is there a shape below this row?
            If (j > 3) Then  ' Is the shape not at the start of the tree?
                ' Not OK to delete row above a shape, unless it is at the start of the tree
                If (ws.Shapes(j).TopLeftCell.Column - ws.Shapes(1).TopLeftCell.Column > 2) Then
                    delete_ro = 0
                End If
            
            End If
        End If
        
        If ro_check = i - 1 Then ' Is there a shape above the row?
            
            p = InStr(ws.Shapes(j).Name, "TrNd")
            
            If (p > 0) Then  ' Is the shape a decision or uncertainty node?
                '  If a node, then must move the E-value and CE values so that they do not get deleted with row
                ws.Cells(ro_check + 1, ws.Shapes(j).TopLeftCell.Column).Cut
                ActiveSheet.Paste Destination:=ws.Cells(ro_check - 2, ws.Shapes(j).TopLeftCell.Column)
                
                ws.Cells(ro_check + 1, ws.Shapes(j).TopLeftCell.Column - 1).Cut
                ActiveSheet.Paste Destination:=ws.Cells(ro_check - 2, ws.Shapes(j).TopLeftCell.Column - 1)
                ' If at edge of sheet, do not try to cut at Row 0, otherwise, go ahead
                If ws.Shapes(j).TopLeftCell.Column > 2 Then
                    ws.Cells(ro_check + 1, ws.Shapes(j).TopLeftCell.Column - 2).Cut
                    ActiveSheet.Paste Destination:=ws.Cells(ro_check - 2, ws.Shapes(j).TopLeftCell.Column - 2)
                End If
                
                replace_needed = 1 ' We moved some values so rememeber that
            End If
        End If
    
    Next j
    ' Is it still OK to delete Row?
    If delete_ro = 1 Then
        Rows(ii & ":" & ii).Select
        Selection.Delete Shift:=xlUp
    End If
    
    ' If we moved values, we must put them back below the node where we found them
    If replace_needed = 1 Then
        For j = 1 To ws.Shapes.Count
        ro_check = ws.Shapes(j).TopLeftCell.Row
            If ro_check = i - 1 Then
                p = InStr(ws.Shapes(j).Name, "TrNd")
                If (p > 0) Then
                    
                    ws.Cells(ro_check - 2, ws.Shapes(j).TopLeftCell.Column).Cut
                    ActiveSheet.Paste Destination:=ws.Cells(ro_check + 1, ws.Shapes(j).TopLeftCell.Column)
                    
                    ws.Cells(ro_check - 2, ws.Shapes(j).TopLeftCell.Column - 1).Cut
                    ActiveSheet.Paste Destination:=ws.Cells(ro_check + 1, ws.Shapes(j).TopLeftCell.Column - 1)
                    
                    If ws.Shapes(j).TopLeftCell.Column > 2 Then
                        ws.Cells(ro_check - 2, ws.Shapes(j).TopLeftCell.Column - 2).Cut
                        ActiveSheet.Paste Destination:=ws.Cells(ro_check + 1, ws.Shapes(j).TopLeftCell.Column - 2)
                    End If
                    
                    replace_needed = 0
                End If
            End If
        Next j
    End If
    
Next i
    
    ' Put the Tree CE value back where we found it
    j = 1
    ro_check = ws.Shapes(j).TopLeftCell.Row
    ws.Cells(ro_check + 1, ws.Shapes(j).TopLeftCell.Column + 1).Cut
    ActiveSheet.Paste Destination:=ws.Cells(ro_check + 2, ws.Shapes(j).TopLeftCell.Column)


End Function

