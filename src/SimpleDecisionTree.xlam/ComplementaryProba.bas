Attribute VB_Name = "ComplementaryProba"
    'Simple Decision Tree
    'https://bitbucket.org/teachingda/simpledecisiontree
    'Copyright (C) 2008  Thomas Seyller

    'This program is free software: you can redistribute it and/or modify
    'it under the terms of the GNU General Public License as published by
    'the Free Software Foundation, either version 3 of the License, or
    '(at your option) any later version.

    'This program is distributed in the hope that it will be useful,
    'but WITHOUT ANY WARRANTY; without even the implied warranty of
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    'GNU General Public License for more details.

    'You should have received a copy of the GNU General Public License
    'along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    '***************************************************************
    '*** Additional permission under GNU GPL version 3 section 7 ***
    '***************************************************************
    'As a special exception to the terms and conditions of GPL version 3.0,
    'Thomas Seyller hereby grants you the rights to link the
    'macros to functions calls that normally comes with the standard
    'built-in libraries provided by your office application programs.

Public Sub Complementary_Proba()
'When a cell containing a probability is selected, this procedure allows the user to make the value of that probability equal to one minus all the other branch probabilities

    Dim ws As Worksheet
    Set ws = ActiveSheet
    Dim c, cc As Range
    Set c = ActiveCell
    Dim branch As Shape
    
    rw = c.Row
    col = c.Column
    found = False
    'Look for the adjacent branch
    For i = 1 To ws.Shapes.Count
        p = InStr(ws.Shapes(i).Name, "Branch")
        If IsNumeric(p) And p = 1 Then
            If ws.Shapes(i).TopLeftCell.Row = rw + 1 And ws.Shapes(i).TopLeftCell.Column = col Then
                found = True
                Set branch = ws.Shapes(i)
                Exit For
            End If
        End If
    Next i
    If found = True Then
        first_digits_branch_number = Mid(branch.Name, 8, Len(branch.Name) - 8)
        If ws.Shapes(Find_Node(first_digits_branch_number)).AutoShapeType = msoShapeRectangle Then found = False
    End If
    If found = False Then
    'Error message
        Msg = "Please select a cell containing a branch probability."
        MsgBox Msg, vbExclamation, APPNAME
        Exit Sub
    End If
    
    formul = "=1"
    last_digit_this_branch = Right(branch.Name, 1)
    For i = 1 To 9
        If i - last_digit_this_branch <> 0 Then
            j = Find_Branch(first_digits_branch_number & i)
            If j = -1 Then
                Exit For
            Else
                Set cc = ws.Shapes(j).TopLeftCell
                formul = formul & "-" & ws.Cells(cc.Row - 1, cc.Column).Address()
            End If
        End If
    Next i
    
    'If the active cell already contains the formula... it gets "frozen" into its current value
    'Otherwise, the formula gets entered into it
    If ws.Cells(rw, col).Formula = formul Then
        ws.Cells(rw, col).Formula = ws.Cells(rw, col).Value
    Else
        ws.Cells(rw, col).Formula = formul
    End If

End Sub

